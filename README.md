**Lafayette emergency dentist**

Dental emergency is a situation in which a dental practitioner has to be cared immediately. 
At our emergency dentist in Lafayette, Indiana, we understand that you can't always plan when you need our treatment. 
We encourage you to contact us as soon as possible if you have a dental emergency.
Please Visit Our Website [Lafayette emergency dentist](https://dentistlafayetteindiana.com/emergency-dentist.php) for more information. 

---

## Our emergency dentist in Lafayette services

Our Lafayette Indiana Emergency Dentist will arrange you to visit our dentist as soon as we can, so that you will receive the care you need. 
We are happy to take care of a variety of forms of dental emergencies, including: 

High, constant toothache or other forms of tooth pain 
Loss in dental reconstruction, such as missing lining or crown
Harm to soft tissues in the mouth, including the tongue, cheeks, lips and gums. 
A cracked or broken tooth 
A missing or chipped tooth 
A partly or totally knocked-out tooth 
Please call our Lafayette Indiana Emergency Dentist if you have either of these conditions or a type of dental emergency.
